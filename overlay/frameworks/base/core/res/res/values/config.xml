<?xml version="1.0" encoding="utf-8"?>
<!--
/*
** Copyright 2012, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
-->

<!-- These resources are around just to allow their values to be customized
     for different hardware and product builds. -->
<resources>
    <!-- List of regexpressions describing the interface (if any) that represent tetherable
         USB interfaces.  If the device doesn't want to support tething over USB this should
         be empty.  An example would be "usb.*" -->
    <string-array translatable="false" name="config_tether_usb_regexs">
        <item>"usb\\d"</item>
        <item>"rndis\\d"</item>
    </string-array>

    <!-- List of regexpressions describing the interface (if any) that represent tetherable
         Wifi interfaces.  If the device doesn't want to support tethering over Wifi this
         should be empty.  An example would be "softap.*" -->
    <string-array translatable="false" name="config_tether_wifi_regexs">
        <item>"wlan0"</item>
    </string-array>

    <!-- List of regexpressions describing the interface (if any) that represent tetherable
         bluetooth interfaces.  If the device doesn't want to support tethering over bluetooth this
         should be empty. -->
    <string-array translatable="false" name="config_tether_bluetooth_regexs">
           <item>"bt-pan"</item>
    </string-array>

    <!-- Array of allowable ConnectivityManager network types for tethering -->
    <!-- Common options are [1, 4] for TYPE_WIFI and TYPE_MOBILE_DUN or
         [0,1,5,7] for TYPE_MOBILE, TYPE_WIFI, TYPE_MOBILE_HIPRI and TYPE_BLUETOOTH -->
    <integer-array translatable="false" name="config_tether_upstream_types">
        <item>0</item>
        <item>1</item>
        <item>5</item>
        <item>7</item>
    </integer-array>

    <!-- This string array should be overridden by the device to present a list of network
                  attributes.  This is used by the connectivity manager to decide which networks can coexist
         based on the hardware -->
    <!-- An Array of "[Connection name],[ConnectivityManager.TYPE_xxxx],
                  [associated radio-type],[priority],[restoral-timer(ms)],[dependencyMet]  -->
    <!-- the 5th element "resore-time" indicates the number of milliseconds to delay
                  before automatically restore the default connection.  Set -1 if the connection
         does not require auto-restore. -->
    <!-- the 6th element indicates boot-time dependency-met value. -->
    <string-array translatable="false" name="networkAttributes">
        <item>"wifi,1,1,1,-1,true"</item>
        <item>"mobile,0,0,0,-1,true"</item>
        <item>"mobile_mms,2,0,2,60000,true"</item>
        <item>"mobile_supl,3,0,2,60000,true"</item>
        <item>"mobile_hipri,5,0,3,60000,true"</item>
        <item>"ethernet,9,9,1,-1,true"</item>
        <item>"bluetooth,7,7,2,-1,true"</item>
        <item>"mobile_fota,10,0,2,60000,true"</item>
        <item>"mobile_ims,11,0,2,60000,true"</item>
        <item>"mobile_cbs,12,0,2,60000,true"</item>
        <item>"wifi_p2p,13,1,0,-1,true"</item>
    </string-array>

    <!-- This string array should be overridden by the device to present a list of radio
                  attributes.  This is used by the connectivity manager to decide which networks can coexist
         based on the hardware -->
    <!-- An Array of "[ConnectivityManager connectionType],
                               [# simultaneous connection types]"  -->
    <string-array translatable="false" name="radioAttributes">
        <item>"1,1"</item>
        <item>"0,1"</item>
        <item>"7,1"</item>
        <item>"9,1"</item>
    </string-array>

    <!-- Set this to true to enable the platform's auto-power-save modes like doze and
         app standby.  These are not enabled by default because they require a standard
         cloud-to-device messaging service for apps to interact correctly with the modes
         (such as to be able to deliver an instant message to the device even when it is
         dozing).  This should be enabled if you have such services and expect apps to
         correctly use them when installed on your device.  Otherwise, keep this disabled
         so that applications can still use their own mechanisms. -->
    <bool name="config_enableAutoPowerModes">false</bool>

    <!-- Default list of files pinned by the Pinner Service -->
    <string-array translatable="false" name="config_defaultPinnerServiceFiles">
        <item>"/system/framework/x86_64/boot-framework.oat"</item>
        <item>"/system/framework/framework.jar"</item>
        <item>"/system/framework/oat/x86_64/services.odex"</item>
        <item>"/system/framework/services.jar"</item>
        <item>"/system/framework/x86_64/boot.oat"</item>
        <item>"/system/framework/x86_64/boot-core-libart.oat"</item>
        <item>"/apex/com.android.runtime/javalib/core-oj.jar"</item>
        <item>"/apex/com.android.runtime/javalib/core-libart.jar"</item>
        <item>"/apex/com.android.media/javalib/updatable-media.jar"</item>
        <item>"/product/priv-app/SystemUI/SystemUI.apk"</item>
        <item>"/product/priv-app/SystemUI/oat/x86_64/SystemUI.odex"</item>
        <item>"/system/lib64/libsurfaceflinger.so"</item>
    </string-array>

    <!-- Should the pinner service pin the Home application? -->
    <bool name="config_pinnerHomeApp">true</bool>

    <!-- List of files pinned by the Pinner Service with the apex boot image b/119800099 -->
    <string-array translatable="false" name="config_apexBootImagePinnerServiceFiles">
        <item>"/system/framework/framework.jar"</item>
        <item>"/system/framework/services.jar"</item>
        <item>"/apex/com.android.runtime/javalib/core-oj.jar"</item>
        <item>"/apex/com.android.runtime/javalib/core-libart.jar"</item>
        <item>"/apex/com.android.media/javalib/updatable-media.jar"</item>
        <item>"/system/priv-app/SystemUI/SystemUI.apk"</item>
        <item>"/system/lib64/libsurfaceflinger.so"</item>
    </string-array>

    <!-- Flag indicating whether the we should enable the automatic brightness in Settings.
         Software implementation will be used if config_hardware_auto_brightness_available is not set -->
    <bool name="config_automatic_brightness_available">true</bool>
    <!-- Array of light sensor LUX values to define our levels for auto backlight brightness support.
         The N entries of this array define N + 1 zones as follows:
         Zone 0:        0 <= LUX < array[0]
         Zone 1:        array[0] <= LUX < array[1]
         ...
         Zone N:        array[N - 1] <= LUX < array[N]
         Zone N + 1:    array[N] <= LUX < infinity
         Must be overridden in platform specific overlays -->
    <integer-array name="config_autoBrightnessLevels">
        <item>50</item>
        <item>300</item>
        <item>720</item>
        <item>2000</item>
    </integer-array>

    <!-- Array of output values for LCD backlight corresponding to the LUX values
         in the config_autoBrightnessLevels array.  This array should have size one greater
         than the size of the config_autoBrightnessLevels array.
         This must be overridden in platform specific overlays -->
    <integer-array name="config_autoBrightnessLcdBacklightValues">
        <item>60</item>
        <item>100</item>
        <item>140</item>
        <item>200</item>
        <item>250</item>
    </integer-array>

    <!-- Sets whether menu shortcuts should be displayed on panel menus when
                  a keyboard is present. -->
    <bool name="config_showMenuShortcutsWhenKeyboardPresent">true</bool>

    <!-- Whether a software navigation bar should be shown. NOTE: in the future this may be
         autodetected from the Configuration. -->
    <bool name="config_showNavigationBar">true</bool>

    <!--  Maximum number of supported users -->
    <integer name="config_multiuserMaximumUsers">8</integer>
    <!--  Whether UI for multi user should be shown -->
    <bool name="config_enableMultiUserUI">true</bool>

    <!-- When true use the linux /dev/input/event subsystem to detect the switch changes
         on the headphone/microphone jack. When false use the older uevent framework. -->
    <bool name="config_useDevInputEventForAudioJack">true</bool>

    <!-- Flag indicating that this device does not rotate and will always remain in its default
         orientation. Activities that desire to run in a non-compatible orientation will be run
         from an emulated display within the physical display. -->
    <bool name="config_forceDefaultOrientation">false</bool>

    <!-- The device supports freeform window management. Windows have title bars and can be moved
         and resized. If you set this to true, you also need to add
         PackageManager.FEATURE_FREEFORM_WINDOW_MANAGEMENT feature to your device specification.
         The duplication is necessary, because this information is used before the features are
         available to the system.-->
    <bool name="config_freeformWindowManagement">true</bool>

    <!-- Controls the opacity of the navigation bar depending on the visibility of the
         various workspace stacks.
         0 - Nav bar is always opaque when either the freeform stack or docked stack is visible.
         1 - Nav bar is always translucent when the freeform stack is visible, otherwise always
         opaque.
         -->
    <integer name="config_navBarOpacityMode">0</integer>
    
    <!-- Indicate whether the lid state impacts the accessibility of
         the physical keyboard.  0 means it doesn't, 1 means it is accessible
         when the lid is open, 2 means it is accessible when the lid is
         closed.  The default is 0. -->
    <integer name="config_lidKeyboardAccessibility">1</integer>
    
    <!-- Indicate whether closing the lid causes the lockscreen to appear.
         The default is false. -->
    <bool name="config_lidControlsScreenLock">true</bool>
    
    <!-- Indicate whether closing the lid causes the device to go to sleep and opening
         it causes the device to wake up.
         The default is false. -->
    <bool name="config_lidControlsSleep">true</bool>

    <!-- The list of components which should be automatically disabled for a specific device. -->
    <string-array name="config_deviceDisabledComponents" translatable="false">
        <item>com.google.android.gms/com.google.android.gms.nearby.messages.service.NearbyMessagesService</item>
    </string-array>
    
    <!-- Disable rounded corners on windows to improve graphics performance -->
    <bool name="config_supportsRoundedCornersOnWindows">false</bool>
    
    <!-- Boolean indicating whether the HWC setColorTransform function can be performed efficiently
         in hardware. -->
    <!-- TODO: This is not actually performed in hardware at the moment.
         However, disabling this breaks LiveDisplay. -->
    <bool name="config_setColorTransformAccelerated">true</bool>        
</resources>
